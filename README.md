**Readme**

Pagina oficial de la iglesia monte sion Ecuador*

---

# Herramientas

Herramientas, paquetes, etc. Utilizados en este proyecto

# 1 lite-server

servidor web  [lite-server](https://github.com/johnpapa/lite-server) *revisar la documentacion*

1. **Instalación**

npm install lite-server --save-dev

*Agregar en "script" dentro del package.json* 

2. Dentro de package.json ...
   " scripts " : {
     " dev " : " lite-server " 
  },
3. **Ejecutar**

npm ejecutar dev


# 2. Bootstrap

**Instalación**  *mediante npm*
npm i bootstrap --save
npm i @popperjs/core --save *dependencia de bootstrap 4.5.3*


---
